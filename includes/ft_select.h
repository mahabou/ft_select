/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maabou-h <maabou-h@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/20 16:04:46 by maabou-h          #+#    #+#             */
/*   Updated: 2019/08/20 16:05:02 by maabou-h         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SELECT_H
# define FT_SELECT_H

# include <unistd.h>
# include <sys/ioctl.h>
# include <stdlib.h>
# include <term.h>
# include <curses.h>

# define ERROR_MSG1 "Problem setting up tty"
# define ERROR_MSG2 "Problem retrieving TERM environment variable"
# define ERROR_MSG3 "Incomplete database for terminal type"
# define ERROR_MSG4 "Database not found for terminal type"
# define ERROR_MSG5 "Error while filling termios structure"
# define ERROR_MSG6 "Caught signal, exiting now..."
# define S_REG (1 << 0)
# define S_CUR (1 << 1)
# define S_DEL (1 << 2)
# define S_ACT (1 << 3)
# define S_TCS 5

struct s_env	g_env;

typedef struct	s_arg
{
	char			*name;
	struct s_arg	*next;
	unsigned char	status;
}				t_arg;

typedef struct	s_env
{
	struct termios	termios;
	struct winsize	win;
	int				ret;
	int				nbl;
	int				ws;
	int				items;
	int				cursor;
	int				fd;
	char			buf[5];
	char			*type;
	struct s_arg	*list;
}				t_env;

void			signal_handler(int sig);
void			signal_handler_ctrlz(int sig);
void			signal_handler_fg(int sig);
void			signal_handler_wsize(int sig);
void			sigcheck(void);
void			free_list(t_arg **list);
void			init_args(t_env *env, char **argv);
int				ft_putc(int c);
void			die();
void			reset_term(t_env *env);
void			init_term(t_env *env);
void			set_mode(t_env *env);
void			set_items(t_env *env);
void			ft_bzero(void *s, size_t n);
size_t			ft_strlen(const char *s);
void			ft_putchar_fd(char c, int fd);
void			ft_putstr_fd(char const *s, int fd);
void			ft_putendl_fd(char const *s, int fd);

#endif
