/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_items.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maabou-h <maabou-h@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 11:06:11 by maabou-h          #+#    #+#             */
/*   Updated: 2019/08/20 15:08:42 by maabou-h         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static void
	printitembis(t_env *env, t_arg *arg)
{
	if ((arg->status & S_CUR))
		ft_putstr_fd("\033[4m", env->fd);
	if ((arg->status & S_ACT))
		ft_putstr_fd("\033[47;4;30m", env->fd);
	if (!(arg->status & S_DEL))
		ft_putstr_fd(arg->name, env->fd);
	if ((arg->status & S_CUR) || (arg->status & S_ACT))
		ft_putstr_fd("\033[0m", env->fd);
}

static void
	print_items(t_env *env, int col, int wordsize)
{
	t_arg	*lst;
	int		j;
	int		k;

	lst = env->list;
	while (lst)
	{
		j = -1;
		while (lst && ++j < col)
		{
			if (!(lst->status & S_DEL))
			{
				k = wordsize - ft_strlen(lst->name) + 2;
				printitembis(env, lst);
				while (--k)
					ft_putchar_fd(' ', env->fd);
			}
			else
				j--;
			lst = lst->next;
		}
		ft_putchar_fd('\n', env->fd);
	}
}

static void
	init_items(t_env *env, int wordsize, int line)
{
	int	col;

	col = 1;
	while ((col * (env->win.ws_row - 1)) < line)
		col++;
	print_items(env, col, wordsize);
	env->items = line;
	env->nbl = col;
	env->ws = wordsize;
}

void
	set_items(t_env *env)
{
	t_arg	*list;
	int		longest[3];

	list = env->list;
	longest[0] = 0;
	longest[1] = 0;
	longest[2] = 0;
	while (list)
	{
		if (!(list->status & S_DEL)
			&& (longest[0] = ft_strlen(list->name)) >= longest[1])
			longest[1] = longest[0];
		if (!(list->status & S_DEL))
			longest[2]++;
		list = list->next;
	}
	init_items(env, longest[1], longest[2]);
}
