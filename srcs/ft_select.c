/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maabou-h <maabou-h@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/14 14:23:38 by maabou-h          #+#    #+#             */
/*   Updated: 2019/08/20 15:58:18 by maabou-h         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static void
	handle_wrap(t_env *env, int *i, int *k, int *m)
{
	if (env->buf[2] && (env->buf[2] == 'A' || env->buf[2] == 'D'))
		*i = (*i - ((env->buf[2] == 'D') ? 1 : env->nbl)) % env->items;
	if (env->buf[2] && (env->buf[2] == 'C' || env->buf[2] == 'B'))
		*i = (*i + ((env->buf[2] == 'B') ? env->nbl : 1)) % env->items;
	if (env->buf[0] == ' ' && !env->buf[1])
		*k = *i;
	if (env->buf[0] == 'r' && !env->buf[1])
		*m = *i;
	*i %= env->items;
	if (*i < 0)
		*i += env->items;
}

static void
	handle_loop(t_arg *lst, int i, int *j, int k)
{
	if (lst->status & S_CUR)
		lst->status -= S_CUR;
	if (lst->status & S_DEL)
		*j = *j - 1;
	if (*j == i)
		lst->status += S_CUR;
	if (*j == k)
	{
		if (!(lst->status & S_ACT))
			lst->status += S_ACT;
		else
			lst->status -= S_ACT;
	}
	*j = *j + 1;
}

static void
	handle_list(t_env *env, int k, int l, int m)
{
	static int	i = 0;
	int			j;
	t_arg		*lst;

	lst = env->list;
	if (env->buf[0] == 127 || (env->buf[2] && env->buf[2] == '3'))
		l = i;
	if (env->buf[0] == 127 || (env->buf[2] && env->buf[2] == '3'))
		i += (i == env->items - 1);
	handle_wrap(env, &i, &k, &m);
	j = 0;
	while (lst)
	{
		if (i == m)
			lst->status = S_REG;
		if (j == l && lst->status & S_CUR && !(lst->status & S_DEL))
		{
			lst->status = S_DEL;
			env->items--;
		}
		handle_loop(lst, i, &j, k);
		lst = lst->next;
	}
	tputs(tgetstr("vi", NULL), 1, ft_putc);
	tputs(tgetstr("cl", NULL), 1, ft_putc);
}

static void
	print_list(t_env *env)
{
	t_arg	*tmp;
	int		i;

	tmp = env->list;
	i = 0;
	while (tmp)
	{
		if (tmp->status & S_ACT)
		{
			if (i > 0)
				ft_putchar_fd(' ', STDOUT_FILENO);
			ft_putstr_fd(tmp->name, STDOUT_FILENO);
			i++;
		}
		tmp = tmp->next;
	}
	ft_putchar_fd('\n', STDOUT_FILENO);
}

int
	main(int argc, char **argv)
{
	if (argc > 1)
	{
		sigcheck();
		init_term(&g_env);
		init_args(&g_env, argv);
		set_mode(&g_env);
		while ((g_env.ret = read(g_env.fd, &g_env.buf, S_TCS)))
		{
			handle_list(&g_env, -1, -1, -1);
			set_items(&g_env);
			if (g_env.items == 0 || (g_env.buf[0] == 27 && !g_env.buf[1])
				|| (g_env.buf[0] == '\n' && !g_env.buf[1]))
			{
				reset_term(&g_env);
				if (g_env.items > 0 && !(g_env.buf[0] == 27 && !g_env.buf[1]))
					print_list(&g_env);
				break ;
			}
			ft_bzero(g_env.buf, S_TCS);
		}
		free_list(&g_env.list);
	}
	else
		ft_putendl_fd("ft_select: not enough arguments", STDERR_FILENO);
	return (0);
}
