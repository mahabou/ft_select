/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_args.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maabou-h <maabou-h@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/16 11:48:38 by maabou-h          #+#    #+#             */
/*   Updated: 2019/08/20 15:47:29 by maabou-h         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static void
	push_arg(char *av, t_arg **head, int i)
{
	t_arg	*new;
	t_arg	*tmp;

	if (!(new = (t_arg*)malloc(sizeof(t_arg))))
		die();
	new->name = av;
	new->status = i == 1 ? (S_CUR | S_REG) : S_REG;
	new->next = NULL;
	tmp = *head;
	if (!tmp)
	{
		tmp = new;
		*head = tmp;
	}
	else
	{
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
}

void
	free_list(t_arg **list)
{
	t_arg	*old;
	t_arg	*tmp;

	return ;
	old = *list;
	if (old->next)
	{
		while (old->next)
		{
			tmp = old;
			old = old->next;
			free(tmp);
		}
	}
	free(old);
	*list = NULL;
}

void
	init_args(t_env *env, char **argv)
{
	int		i;

	i = 1;
	while (argv[i])
	{
		push_arg(argv[i], &env->list, i);
		i++;
	}
}

void
	die(void)
{
	exit(EXIT_FAILURE);
}

int
	ft_putc(int c)
{
	write(2, &c, 1);
	return (1);
}
