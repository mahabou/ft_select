/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sig.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maabou-h <maabou-h@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 08:44:15 by maabou-h          #+#    #+#             */
/*   Updated: 2019/08/20 15:47:44 by maabou-h         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void
	signal_handler(int sig)
{
	if (sig == SIGWINCH)
		signal_handler_wsize(sig);
	else if (sig == SIGTSTP || sig == SIGSTOP)
		signal_handler_ctrlz(sig);
	else if (sig == SIGCONT)
		signal_handler_fg(sig);
	else if (sig == SIGABRT || sig == SIGBUS || sig == SIGSEGV || sig == SIGILL
		|| sig == SIGKILL || sig == SIGINT || sig == SIGHUP || sig == SIGTRAP
		|| sig == SIGSYS || sig == SIGEMT || sig == SIGFPE || sig == SIGPIPE)
	{
		reset_term(&g_env);
		ft_putendl_fd(ERROR_MSG6, STDERR_FILENO);
		exit(0);
	}
	else
		signal(sig, SIG_DFL);
}

void
	signal_handler_ctrlz(int sig)
{
	ioctl(g_env.fd, TIOCSTI, "\x1A");
	reset_term(&g_env);
	tputs(tgetstr("cl", NULL), 1, ft_putc);
	tputs(tgetstr("ve", NULL), 1, ft_putc);
	tputs(tgetstr("rc", NULL), 1, ft_putc);
	signal(sig, SIG_DFL);
}

void
	signal_handler_fg(int sig)
{
	reset_term(&g_env);
	init_term(&g_env);
	set_mode(&g_env);
	(void)sig;
}

void
	signal_handler_wsize(int sig)
{
	tputs(tgetstr("vi", NULL), 1, ft_putc);
	tputs(tgetstr("cl", NULL), 1, ft_putc);
	set_mode(&g_env);
	(void)sig;
}

void
	sigcheck(void)
{
	int	sig;

	sig = -1;
	while (++sig < 32)
		signal(sig, signal_handler);
}
