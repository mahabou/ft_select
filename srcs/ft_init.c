/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maabou-h <maabou-h@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/14 15:21:38 by maabou-h          #+#    #+#             */
/*   Updated: 2019/08/20 15:48:53 by maabou-h         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"
#include <fcntl.h>

void
	set_mode(t_env *env)
{
	ft_bzero(g_env.buf, S_TCS);
	if (tcgetattr(env->fd, &env->termios) < 0)
	{
		ft_putendl_fd(ERROR_MSG5, STDERR_FILENO);
		die(env);
	}
	env->termios.c_lflag &= ~(ICANON | ECHO);
	env->termios.c_cc[VMIN] = 1;
	env->termios.c_cc[VTIME] = 0;
	if (tcsetattr(env->fd, TCSADRAIN, &env->termios) < 0)
	{
		ft_putendl_fd(ERROR_MSG5, STDERR_FILENO);
		die(env);
	}
	ioctl(STDERR_FILENO, TIOCGWINSZ, &env->win);
	tputs(tgetstr("vi", NULL), 1, ft_putc);
	tputs(tgetstr("cl", NULL), 1, ft_putc);
	tputs(tgetstr("sc", NULL), 1, ft_putc);
	set_items(&g_env);
}

void
	reset_term(t_env *env)
{
	if (tcgetattr(env->fd, &env->termios) < 0)
	{
		ft_putendl_fd(ERROR_MSG5, STDERR_FILENO);
		die(env);
	}
	env->termios.c_lflag |= (ICANON | ECHO);
	if (tcsetattr(env->fd, TCSANOW, &env->termios) < 0)
	{
		ft_putendl_fd(ERROR_MSG5, STDERR_FILENO);
		die(env);
	}
	tputs(tgetstr("cl", NULL), 1, ft_putc);
	tputs(tgetstr("ve", NULL), 1, ft_putc);
	tputs(tgetstr("rc", NULL), 1, ft_putc);
	close(env->fd);
}

void
	init_term(t_env *env)
{
	if ((env->fd = open(ttyname(STDIN_FILENO), O_RDWR)) == -1)
	{
		ft_putendl_fd(ERROR_MSG1, STDERR_FILENO);
		die(env);
	}
	if (!isatty(env->fd))
	{
		ft_putendl_fd(ERROR_MSG1, STDERR_FILENO);
		die(env);
	}
	if (!(env->type = getenv("TERM")))
	{
		ft_putendl_fd(ERROR_MSG2, STDERR_FILENO);
		die(env);
	}
	if (!(env->ret = tgetent(NULL, env->type)))
	{
		ft_putendl_fd(!env->ret ? ERROR_MSG3 : ERROR_MSG4, STDERR_FILENO);
		die(env);
	}
}
