# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sdincbud <sdincbudstudent.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/02/13 18:42:02 by maabou-h          #+#    #+#              #
#    Updated: 2019/08/16 15:04:04 by maabou-h         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

### COLORS ###
NOC         = \033[0m
BOLD        = \033[1m
UNDERLINE   = \033[4m
RED         = \033[0;31m
GREEN       = \033[0;32m
YELLOW      = \033[0;33m
BLUE        = \033[0;34m
VIOLET      = \033[0;35m
WHITE       = \033[0;37m

SRC_PATH = srcs

SRC_NAME = 	ft_select.c	\
			ft_init.c	\
			ft_args.c	\
			ft_sig.c	\
			ft_items.c	\
			ft_utils.c

NAME = ft_select

LDFLAGS = -ltermcap

CC = clang

CFLAGS = -Iincludes -Werror -Wall -Wextra

OBJ_PATH = .obj

OBJ_NAME = $(SRC_NAME:%.c=%.o)
	OBJ = $(addprefix $(OBJ_PATH)/,$(OBJ_NAME))
	SRC = $(addprefix $(SRC_PATH)/,$(SRC_NAME))

all: $(NAME)

$(NAME): $(OBJ)
	@$(CC) $(LDFLAGS) $^ -o $@
	@printf "$(GREEN)Project successfully compiled$(NOC)"

$(OBJ): includes/ft_select.h

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@mkdir -p $(OBJ_PATH)
	@$(CC) $(CFLAGS) -o $@ -c $<
	@printf "$(BLUE)Creating object file -> $(WHITE)$(notdir $@)... $(RED)[Done]\n"

clean:
	@printf "$(RED)Supressing libraries files$(RED)\n"
	@rm -rfv $(OBJ_PATH)

fclean: clean
	@rm -fv $(NAME)

re: fclean all

.PHONY: all, clean, fclean, re
